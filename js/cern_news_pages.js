(function ($, Drupal) {

  'use strict';
    
  $("form").submit(function(e){
    var format = $('#edit-field-p-news-display-news-format option:selected');
    var values="Media";
    if(format.text() == "News") {
      $.each(values.split(","), function(i,e){
        $("#edit-field-p-news-display-audience option:contains("+ e +")").prop("selected", true);
      });
    }
  });

})(jQuery, Drupal);